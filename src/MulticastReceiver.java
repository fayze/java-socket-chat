import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.*;

public class MulticastReceiver extends Thread {
    protected MulticastSocket socket = null;
    protected byte[] buf = new byte[1024];
    private final boolean resend;

    public MulticastReceiver(boolean resend){
//            buf = "salut".getBytes();
        this.resend = resend;
    }

    public void run() {
        InetAddress group = null;
        try {
            socket = new MulticastSocket(40446);
            group = InetAddress.getByName("224.168.1.1");
            socket.joinGroup(group);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (true) {
            System.out.println("length: " + buf.length);
            DatagramPacket packet = new DatagramPacket(buf, buf.length);
            try {
                socket.receive(packet);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(
                    "multicast packet ip: [" + packet.getAddress().getHostAddress() +
                    "] packet port: [" + packet.getSocketAddress() +
                    "] packetOffset: [" + packet.getOffset() +
                    "] packet size: [" + packet.getLength() + "]");
            String received = new String( packet.getData(), packet.getOffset(), packet.getLength());

                System.out.println("multicast Received [" + received + "]");
                if (received.equals("end"))
                    break;
                else if(resend) {
                    try {
                        send(received,packet.getAddress());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
        }
        try {
            socket.leaveGroup(group);
        } catch (IOException e) {
            e.printStackTrace();
        }
        socket.close();
    }


    public void send(String msg, InetAddress dest) throws IOException{
        DatagramSocket socket;
        byte[] buf;
        socket = new DatagramSocket();
        buf = msg.getBytes();
        DatagramPacket packet = new DatagramPacket(buf, buf.length, dest, 40446);
        socket.send(packet);
        socket.close();
    }
}