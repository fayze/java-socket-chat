import java.io.IOException;
import java.net.*;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

import Model.Message;
import Model.Signal;
import Model.Type;

public class Main implements Observer {
    public boolean bool = true;
    public String username = "";

    public ConnexionManager server = new ConnexionManager(ConnexionManager.ManagerMode.BROADCAST);

    public Main(){
        server.addObserver(this);
    }

    @Override
    public void update(Observable observable, Object o) {
        if(observable.getClass() == ConnexionManager.class){
            Signal s = (Signal) o;
            if(s.type == Type.BAD_USERNAME){
                System.out.println("s.type = [" + s.type.toString() + "]");
            }
            else if(s.type == Type.GOOD_USERNAME) {
                bool = false;
                System.out.println("s.type = [" + s.type.toString() + "]");

                System.out.println("Username [" + this.username + "] is valide");
                server.setClientName(username);
                (new Thread(server)).start();
                String msg = "";

                Scanner sc = new Scanner(System.in);
                server.printUsers();

                while(!msg.equals("exit")){
                    msg = sc.nextLine();
                    if(msg.contains(":")) {
                        String uname = msg.split(":")[0];
                        msg = msg.split(":")[1];
                        if(!server.sendMessage(new Message(username, Calendar.getInstance().getTime(),msg,uname)))
                            System.out.println( uname + " Not Sended");
                        //server.printUsers();
                    }
                }
                server.setWork(false);
                sc.close();
                System.exit(0);
            }
        }
    }

    public static void main(String[] args) throws IOException {
        Main m = new Main();
       // Scanner sc = new Scanner(System.in);

       // while(m.bool) {
            System.out.print("\nEnter your username: ");
            m.username = "a";
            //m.username  =  ThreadLocalRandom.current().nextInt(100) + "";
           // m.username = sc.nextLine();
           // sc.close();
            m.server.isUsed(m.username);
//            if(m.bool)
//                System.out.println("This Username is already taken, choose an other one");
//        }




//
//        try(final DatagramSocket socket = new DatagramSocket()){
//            socket.connect(InetAddress.getByName(""), 30000);
//            System.out.println("args = [" + socket.getLocalAddress().getHostAddress() + "]");
//        }
//        int mode = 1;
//        System.out.println("Enter the functionning mode: ");
//        mode = sc.nextInt();
//        sc.nextLine();
//
//        if(mode == 1){
//            MulticastReceiver multicastReceiver = new MulticastReceiver(true);
//            multicastReceiver.start();
//
//            (new Thread(new BroadcastReceiver())).start();
//        }else if (mode == 2){
//            List<InetAddress> l = listAllBroadcastAddresses();
//            for (InetAddress i : l) {
//                System.out.println("adresse = [" + i + "]");
//                broadcast("test",i);
//            }
//            MulticastReceiver multicastReceiver = new MulticastReceiver(false);
//            multicastReceiver.start();
//            String str = "";
//
//            while(!"exit".equals(str)) {
//                System.out.println("Entrer le message à multicaster: ");
//                str = sc.nextLine();
//                multicast(str);
//            }
//        }
//        else{
//            System.out.println("mauvais choix");
//        }


    }

    public static List<InetAddress> listAllBroadcastAddresses() throws SocketException {
        List<InetAddress> broadcastList = new ArrayList<>();
        Enumeration<NetworkInterface> interfaces
                = NetworkInterface.getNetworkInterfaces();
        int dsd;
        while (interfaces.hasMoreElements()) {
            NetworkInterface networkInterface = interfaces.nextElement();
            int fbbgn;
            if (networkInterface.isLoopback() || !networkInterface.isUp()) {
                continue;
            }
            int fgh;
            networkInterface.getInterfaceAddresses().stream()
                    .map(a -> a.getBroadcast())
                    .filter(Objects::nonNull)
                    .forEach(broadcastList::add);
        }
        return broadcastList;
    }

    public static void multicast(String multicastMessage) throws IOException {
        DatagramSocket socket;
        InetAddress group;
        byte[] buf;
        socket = new DatagramSocket();
        group = InetAddress.getByName("224.168.1.1");
        buf = multicastMessage.getBytes();
        System.out.println("multicastMessage = [" + multicastMessage + "] buf = [" + buf + "]");
        DatagramPacket packet = new DatagramPacket(buf, buf.length, group, 40446);
        socket.send(packet);
        socket.close();
    }

    public static void broadcast(String broadcastMessage, InetAddress address) throws IOException {
        DatagramSocket socket = new DatagramSocket();
        socket.setBroadcast(true);
        byte[] buffer = broadcastMessage.getBytes();
        int asdsd;

        DatagramPacket packet
                = new DatagramPacket(buffer, buffer.length, address, 4445);
        socket.send(packet);
        socket.close();
    }

}
