import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;


/**
 *                                          Scénario
 * Démarrer serveur => scan et récupération des ports et des username uniquement pas de sockets
 *      => Username%&%scan
 * Envoie de Message
 *      => initConnection si première communication
 *      => sauvegarde des sockets
 *      => ajout de listeners au niveau des deux clients
 *      => envoie de message
 *      => reception au niveau de l'autre côté
 *      => traitment du message recu (à voir plus tard ***********************)
 *
 *
 * Ok Reste a corriger le bug de contact par l'autre (différence de port en recption et en emission) => mettre a jour connectedUsers avec la nouvelle valeure du port
 *
 *
 * QCM
 * Shadow stack et concu....
 * Programme a analyser
 *
 * Racoon
 * IP Sec
 * Tunneling
 *
 */



public class ServerTimeOut{/* extends Observable implements Runnable, Observer {

    @Override
    public void update(Observable observable, Object o) {
        if(observable.getClass() == NetworkScanner.class){
            fr.faymir.Model.ScanMessage msg = (fr.faymir.Model.ScanMessage) o;
            if(msg.clients.containsKey(clientName)) {
                networkScanListener = new NetworkScanListener(clientName, msg.clients, msg.newUserVersion, msg.newUserVersion, UUID.randomUUID().toString());
            }
        }
    }

    public enum ManagerMode{
        TEST(1),
        BROADCAST(2);
        private int mode;
        private ManagerMode(int i){
            mode = i;
        }
    }

    private ServerSocket serverSocket = null;
    private String clientName = null;
    private Vector<UserChatListener> friendList;
    private HashMap<String, Integer> connectedUsers;
    private HashMap<String, String> connectedUsers2;
    private static final int portStart = 10000;
    private static final int portEnd = 11000;
    private int server_port = 0;
    private ManagerMode mode = ManagerMode.TEST;
    private NetworkScanner networkScanner;
    private NetworkScanListener networkScanListener;

    private boolean work;

    public ServerTimeOut(ManagerMode mode){
        super();
        this.mode = mode;
        init();
    }

    private void init(){

        work = true;
        friendList = new Vector<>();
        connectedUsers = new HashMap<>();
        connectedUsers2 = new HashMap<>();
        clientName = "%%NONE%%";
        networkScanner = new NetworkScanner("safsdf");
        networkScanner.addObserver(this);
        networkScanListener = null;
        int randomNum = 0;
        while(serverSocket == null){
            try {
                randomNum = ThreadLocalRandom.current().nextInt(portStart, portEnd);
                serverSocket = new ServerSocket(randomNum);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        server_port = randomNum;
        //scanUsers();
        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //System.out.println("Scan finished");
    }

    @Override
    public void run() {
        while (work){
            try {
                Socket socket = serverSocket.accept();
                BufferedReader entreeDepuisClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String msg = entreeDepuisClient.readLine();
                System.out.println("Message: {" + msg + "}");
                this.handleMessage(msg, socket);

            }
            catch (ConnectException e) {
                System.out.println("except");//e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }


    public boolean sendMessage(String username, String str){
        PrintWriter sortieVersClient = null;
        initChat(username);
        UserChatListener u = getFriend(username);
        if(u != null){
            try {
//                System.out.println("getPort() = [" + u.getSocket().getPort() + "], getLocalPort = [" + u.getSocket().getLocalPort() + "]");
                sortieVersClient = new PrintWriter(u.getSocket().getOutputStream());
                sortieVersClient.println(str);
                sortieVersClient.flush();
                System.out.println("Sended!!!");
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            System.out.println("username = [" + username + "], str = [" + str + "], size= [" + friendList.size() + "]\n");
            printUsers();
        }
        return false;
    }

    public void sendMessage(Socket socket, String msg){
        if(socket != null && msg != null){

            PrintWriter sortieVersClient = null;
            try {
                sortieVersClient = new PrintWriter(socket.getOutputStream());
                sortieVersClient.println(msg);
                sortieVersClient.flush();
                System.out.println("Sended!!!");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else
            System.out.println("CANNOT SEND MESSAGE socket = [" + socket + "], msg = [" + msg + "]");
    }

    public boolean initChat(String uname){
        Integer port = null;
        String ip = null;
        port = connectedUsers.get(uname);
        ip = connectedUsers2.get(uname);
        if((port != null || ip!=null) && !isChattingWith(uname)){
            Socket s = null;
            try {
                if(mode == ManagerMode.TEST)
                    s = new Socket("localhost", port);
                else
                    s = new Socket(ip, 11000);

                UserChatListener u = new UserChatListener(uname,s);
                sendMessage(s,clientName + "%&%" + "initConnection");
                (new Thread(u)).start();
                this.friendList.add(u);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    private void handleMessage(String msg, Socket socket){


        * MESSAGES
         * Utilisateur: Salut blabla
         * Servers: [Username]%&%[MessageType]
         *      => initConnection
         *
        if(msg == null || msg.isEmpty()){
            System.out.println("Nothing handled");
        }
        else if (msg.contains("%&%")){
            String uname = msg.split( "%&%")[0];
            String order = msg.split("%&%")[1];

            switch (order){
                case "initConnection": {
                    if(mode == ManagerMode.TEST)
                        this.connectedUsers.put(uname,socket.getPort());
                    else
                        this.connectedUsers2.put(uname, socket.getInetAddress().getAddress().toString());

                    UserChatListener u = new UserChatListener(uname, socket);
                    (new Thread(u)).start();
                    this.friendList.add(u);
                    printUsers();
                }
                    break;
                case "scan":
                    sendMessage(socket,clientName);
                    break;
                case "disconnect":
                    if (mode == ManagerMode.TEST) {
                        this.connectedUsers.remove(uname);
                        UserChatListener u = getFriend(uname);
                        u.setWorking(false);
                        try {
                            u.getSocket().close();
                        } catch (IOException e) {
                            System.out.println("Error when disconnecting: msg = [" + msg + "], socket = [" + socket + "]");
                            e.printStackTrace();
                        }
                        this.friendList.remove(u);
                        System.out.println("User [" + uname + "] disconnected!!");
                        printUsers();
                    }
                    break;
                case "connected":
                    if (mode == ManagerMode.TEST) {
//                    System.out.println("getPort() = [" + socket.getRemoteSocketAddress().toString() + "], getLocalPort = [" + socket.getLocalPort() + "]");
                        this.connectedUsers.put(uname, socket.getPort());
                        try {
                            socket.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        scanUsers();
                        printUsers();
                    }
                    break;
                default:
                    break;
            }
        }
    }

    public void scanUsers(){

        if (mode == ManagerMode.TEST) {
            for (int i = portStart; i < portEnd; i++) {
                if (i != server_port && !connectedUsers.containsValue(i)) {
                    try {
                        Socket socket = new Socket("localhost", i);
                        socket.setSoTimeout(50);
                        BufferedReader entreeDepuisClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                        PrintWriter sortieVersClient = new PrintWriter(socket.getOutputStream());
                        sortieVersClient.println("___NONE___" + "%&%" + "scan");
                        sortieVersClient.flush();
//                    System.out.println("Port " + i + " user number: " + (connectedUsers.size() + 1));
                        String username = entreeDepuisClient.readLine();
//                    System.out.println("Il s'appele: " + username);
                        //User u = new User(username, socket);

                        this.connectedUsers.put(username, i);
                        entreeDepuisClient.close();
                        sortieVersClient.close();

                    } catch (SocketTimeoutException e) {
                        System.out.println("timeout");
                    } catch (ConnectException e) {
//                    System.out.println("not opened");//e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
        }
        else {
            try {
                networkScanner.scanNetwork();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void sendUpdateToFriends(String msg){
        for (UserChatListener chatListener : friendList) {
            sendMessage(chatListener.getSocket(), msg);
        }
    }

    public void sendUpdateToConnected(String msg){

        Set<Map.Entry<String, Integer>> setHm = connectedUsers.entrySet();
        Iterator<Map.Entry<String, Integer>> it = setHm.iterator();

        while(it.hasNext()){

            Map.Entry<String, Integer> e = it.next();
            try {
                Socket s = new Socket("localhost",e.getValue());
                sendMessage(s,msg);
            }
            catch (IOException e1) {
                e1.printStackTrace();
            }

        }
    }

    private boolean isChattingWith(String uname) {
        for (UserChatListener u : friendList) {
            if (u.getNickname().equals(uname))
                return true;
        }
        return false;
    }

    public boolean isUsed(String nickname) {
        scanUsers();

        boolean isUsed = connectedUsers.containsKey(nickname);
        return isUsed;
    }

    public boolean getWork() {
        return work;
    }

    public void setWork(boolean work) {
        this.work = work;
    }

    public UserChatListener getFriend(String username){
        for (UserChatListener user: friendList) {
            if(user.getNickname().compareTo(username) == 0)
                return user;
        }
        return null;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        try {
            serverSocket = new ServerSocket(server_port);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.clientName = clientName;


        this.sendUpdateToConnected(clientName+"%&%"+"connected");
        printUsers();
    }

    public void printUsers(){
        Set<Map.Entry<String, Integer>> setHm = connectedUsers.entrySet();
        Iterator<Map.Entry<String, Integer>> it = setHm.iterator();

        while(it.hasNext()){

            Map.Entry<String, Integer> e = it.next();

            System.out.println("username = [" + e.getKey() + "], port = [" + e.getValue() + "]");

        }
    }*/
}
